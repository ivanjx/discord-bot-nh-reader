**Available Environment Variable Configurations**
- `TOKEN` Your Discord bot's token from Discord Developer Portal.
- `PREFIX` Message prefix to trigger the bot commands.
- `DATA_DIR` Directory location to save bot's data.
- `CACHE_DIR` Directory location to save images cache.
- `FLARESOLVERR_ENDPOINT` Flaresolverr endpoint to bypass CloudFlare security (optional).
- `SEQ_ENDPOINT` Seq endpoint for logging (optional).
- `SEQ_API_KEY` Seq API key for logging (optional).
