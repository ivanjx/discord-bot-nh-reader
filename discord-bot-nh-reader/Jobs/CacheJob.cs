using System;
using System.Threading;
using System.Threading.Tasks;
using discord_bot_nh_reader.Services;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace discord_bot_nh_reader.Jobs;

public class CacheJob : BackgroundService
{
    ICacheService m_cacheService;
    ILogger m_logger;

    public CacheJob(
        ICacheService cacheService,
        ILogger logger)
    {
        m_cacheService = cacheService;
        m_logger = logger;
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        m_logger.Information("Starting cache job");
        
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await m_cacheService.DoCacheJobAsyc(stoppingToken);
                await Task.Delay(
                    TimeSpan.FromSeconds(1),
                    stoppingToken);
            }
            catch (OperationCanceledException) { }
        }

        m_logger.Information("Cache job stopped");
    }
}
