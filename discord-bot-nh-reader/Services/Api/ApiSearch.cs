using System;
using System.Text.Json.Serialization;

namespace discord_bot_nh_reader.Services.Api;

public record ApiSearch
{
    [JsonPropertyName("result")]
    public ApiBook[]? Books
    {
        get;
        set;
    }

    [JsonPropertyName("per_page")]
    public int BookCountPerPage
    {
        get;
        set;
    }
}
