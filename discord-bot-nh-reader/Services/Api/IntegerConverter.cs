using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace discord_bot_nh_reader.Services.Api;

public class IntegerConverter : JsonConverter<int>
{
    public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        try
        {
            return reader.GetInt32();
        }
        catch (Exception)
        {
            string? str = reader.GetString();
            int result;

            if (!int.TryParse(str, out result))
            {
                return 0;
            }
            
            return result;
        }
    }

    public override void Write(Utf8JsonWriter writer, int value, JsonSerializerOptions options)
    {
        writer.WriteNumberValue(value);
    }
}
