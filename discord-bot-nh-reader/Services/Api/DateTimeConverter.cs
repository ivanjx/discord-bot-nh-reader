using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace discord_bot_nh_reader.Services.Api;

public class DateTimeConverter : JsonConverter<DateTime>
{
    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var offset = DateTimeOffset.FromUnixTimeSeconds(reader.GetInt32());
        return offset.UtcDateTime;
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        writer.WriteNumberValue(new DateTimeOffset(value).ToUnixTimeSeconds());
    }
}
