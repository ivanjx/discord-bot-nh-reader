using System;
using System.Text.Json.Serialization;

namespace discord_bot_nh_reader.Services.Api;

public record ApiBookTitle
{
    [JsonPropertyName("pretty")]
    public string? Title
    {
        get;
        set;
    }
}

public record ApiBookTag
{
    [JsonPropertyName("name")]
    public string? Name
    {
        get;
        set;
    }

    [JsonPropertyName("type")]
    public string? Type
    {
        get;
        set;
    }
}

public record ApiBookImage
{
    [JsonPropertyName("t")]
    public string? Type
    {
        get;
        set;
    }

    public string? GetExtension()
    {
        if (Type == "j")
        {
            return "jpg";
        }
        else if (Type == "p")
        {
            return "png";
        }
        else if (Type == "g")
        {
            return "gif";
        }
        else
        {
            return null;
        }
    }
}

public record ApiBookImages
{
    [JsonPropertyName("pages")]
    public ApiBookImage[]? Pages
    {
        get;
        set;
    }

    [JsonPropertyName("cover")]
    public ApiBookImage? Cover
    {
        get;
        set;
    }
}

public record ApiBook
{
    [JsonPropertyName("id")]
    [JsonConverter(typeof(IntegerConverter))]
    public int Id
    {
        get;
        set;
    }

    [JsonPropertyName("media_id")]
    public string? MediaId
    {
        get;
        set;
    }

    [JsonPropertyName("title")]
    public ApiBookTitle? Title
    {
        get;
        set;
    }

    [JsonPropertyName("tags")]
    public ApiBookTag[]? Tags
    {
        get;
        set;
    }

    [JsonPropertyName("images")]
    public ApiBookImages? Images
    {
        get;
        set;
    }

    [JsonPropertyName("upload_date")]
    [JsonConverter(typeof(DateTimeConverter))]
    public DateTime UploadDate
    {
        get;
        set;
    }

    [JsonPropertyName("num_pages")]
    public int PageCount
    {
        get;
        set;
    }

    [JsonPropertyName("num_favorites")]
    public int FavoriteCount
    {
        get;
        set;
    }
}
