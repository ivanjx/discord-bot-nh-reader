using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace discord_bot_nh_reader.Services;

public interface IBookRepository
{
    Task<NhBook?> GetAsync(string id);
    Task AddAsync(NhBook book);
}

public class BookRepository : IBookRepository
{
    const int BOOK_AGE_DAYS = 10;
    
    ISqliteService m_sqliteService;

    public BookRepository(
        ISqliteService sqliteService)
    {
        m_sqliteService = sqliteService;
    }

    long GetEpoch(DateTime time)
    {
        TimeSpan ts = time - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        return (long)ts.TotalSeconds;
    }

    DateTime FromEpoch(long epoch)
    {
        DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        return dt.AddSeconds(epoch);
    }

    string Serialize(string[] input)
    {
        return JsonSerializer.Serialize(
            input,
            BookRepositoryContext.Default.StringArray);
    }

    string[] Deserialize(string input)
    {
        return JsonSerializer.Deserialize(
            input,
            BookRepositoryContext.Default.StringArray) ?? new string[0];
    }

    public async Task AddAsync(NhBook book)
    {
        using SqliteTransaction transaction = m_sqliteService.Connection.BeginTransaction();

        try
        {
            using SqliteCommand deleteCommand = m_sqliteService.Connection.CreateCommand();
            deleteCommand.Transaction = transaction;
            deleteCommand.CommandText = @"
                DELETE FROM books
                WHERE id=$id";
            deleteCommand.Parameters.AddWithValue("id", book.Id);
            deleteCommand.Prepare();
            await deleteCommand.ExecuteNonQueryAsync();

            using SqliteCommand insertCommand = m_sqliteService.Connection.CreateCommand();
            insertCommand.Transaction = transaction;
            insertCommand.CommandText = @"
                INSERT INTO books (
                    id,
                    title,
                    tags,
                    authors,
                    favorite_count,
                    release_date,
                    site_url,
                    cover_image_url,
                    page_image_urls,
                    creation_time
                )
                VALUES(
                    $id,
                    $title,
                    $tags,
                    $authors,
                    $favoriteCount,
                    $releaseDate,
                    $siteUrl,
                    $coverImageUrl,
                    $pageImageUrls,
                    $creationTime
                )";
            insertCommand.Parameters.AddWithValue("$id", book.Id);
            insertCommand.Parameters.AddWithValue("$title", book.Title);
            insertCommand.Parameters.AddWithValue("$tags", Serialize(book.Tags));
            insertCommand.Parameters.AddWithValue("$authors", Serialize(book.Authors));
            insertCommand.Parameters.AddWithValue("$favoriteCount", book.FavoriteCount);
            insertCommand.Parameters.AddWithValue("$releaseDate", GetEpoch(book.ReleaseDate));
            insertCommand.Parameters.AddWithValue("$siteUrl", book.SiteUrl);
            insertCommand.Parameters.AddWithValue("$coverImageUrl", book.CoverImageUrl);
            insertCommand.Parameters.AddWithValue("$pageImageUrls", Serialize(book.PageImageUrls));
            insertCommand.Parameters.AddWithValue("$creationTime", GetEpoch(DateTime.UtcNow));
            insertCommand.Prepare();
            await insertCommand.ExecuteNonQueryAsync();
            
            await transaction.CommitAsync();
        }
        catch
        {
            // Very highly unlikely.
            await transaction.RollbackAsync();
        }
    }

    public async Task<NhBook?> GetAsync(string id)
    {
        using SqliteCommand command = m_sqliteService.Connection.CreateCommand();
        command.CommandText = @"
            SELECT
                title,
                tags,
                authors,
                favorite_count,
                release_date,
                site_url,
                cover_image_url,
                page_image_urls,
                creation_time
            FROM books
            WHERE id=$id";
        command.Parameters.AddWithValue("$id", id);
        command.Prepare();
        using SqliteDataReader reader = await command.ExecuteReaderAsync();

        if (!await reader.ReadAsync())
        {
            return null;
        }

        DateTime creationTime = FromEpoch(reader.GetInt64(8));
        TimeSpan age = DateTime.UtcNow - creationTime;

        if (age > TimeSpan.FromDays(BOOK_AGE_DAYS))
        {
            // Too old.
            return null;
        }

        return new NhBook(
            id,
            reader.GetString(0),
            Deserialize(reader.GetString(1)),
            Deserialize(reader.GetString(2)),
            reader.GetInt32(3),
            FromEpoch(reader.GetInt64(4)),
            reader.GetString(5),
            reader.GetString(6),
            Deserialize(reader.GetString(7)));
    }
}

[JsonSerializable(typeof(string[]))]
public partial class BookRepositoryContext : JsonSerializerContext
{
}
