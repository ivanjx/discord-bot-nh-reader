using System;
using System.Collections.Generic;
using System.Threading;

namespace discord_bot_nh_reader.Services;

public interface IChannelRepository
{
    CancellationToken GetCancellationToken(
        ulong channelId,
        CancellationToken cancellationToken);
    void CancelChannelOperation(ulong channelId);
    void DestroyCancellationToken(ulong channelId);
}

public class ChannelRepository : IChannelRepository
{
    Dictionary<ulong, CancellationTokenSource> m_ctsDict;

    public ChannelRepository()
    {
        m_ctsDict = new Dictionary<ulong, CancellationTokenSource>();
    }

    public CancellationToken GetCancellationToken(ulong channelId, CancellationToken cancellationToken)
    {
        CancellationTokenSource? cts = m_ctsDict.GetValueOrDefault(channelId);

        if (cts != null)
        {
            return cts.Token;
        }

        cts = CancellationTokenSource.CreateLinkedTokenSource(
            cancellationToken);
        m_ctsDict.Add(channelId, cts);
        return cts.Token;
    }

    public void DestroyCancellationToken(ulong channelId)
    {
        CancellationTokenSource? cts = m_ctsDict.GetValueOrDefault(channelId);

        if (cts == null)
        {
            return;
        }

        cts.Dispose();
        m_ctsDict.Remove(channelId);
    }

    public void CancelChannelOperation(ulong channelId)
    {
        CancellationTokenSource? cts = m_ctsDict.GetValueOrDefault(channelId);

        if (cts == null)
        {
            return;
        }

        cts.Cancel();
    }
}
