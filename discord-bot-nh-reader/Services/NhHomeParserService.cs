using System;
using System.Linq;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;

namespace discord_bot_nh_reader.Services;

public interface INhHomeParserService
{
    Task<string[]> GetBookIdsAsync(string homeHtml);
}

public class NhHomeParserService : INhHomeParserService
{
    public async Task<string[]> GetBookIdsAsync(string homeHtml)
    {
        IConfiguration config = Configuration.Default;
        IBrowsingContext context = BrowsingContext.New(config);
        IDocument doc = await context.OpenAsync(
            req => req.Content(homeHtml));
        IElement? indexPopularElem = doc.All
            .Where(x => x.ClassList.Contains("index-popular"))
            .FirstOrDefault();

        if (indexPopularElem == null)
        {
            return new string[0];
        }

        IElement[] galleries = indexPopularElem.Children
            .Where(x => x.ClassList.Contains("gallery"))
            .ToArray();
        IElement[] covers = galleries
            .Select(x => x.Children.Where(xx => xx.ClassList.Contains("cover")).FirstOrDefault())
            .Where(x => x != null)
            .ToArray()!;
        return covers
            .Select(x => x.GetAttribute("href"))
            .Where(x => !string.IsNullOrEmpty(x))
            .Select(x => x!.Split('/', StringSplitOptions.RemoveEmptyEntries).Last())
            .ToArray();
    }
}
