using System;
using System.Threading;

namespace discord_bot_nh_reader.Services;

public record CacheJob(
    NhBook Book,
    int Page);

public class CacheState
{
    public CacheJob[] Jobs
    {
        get;
        set;
    }

    public SemaphoreSlim Semaphore
    {
        get;
    }

    public CacheState()
    {
        Jobs = Array.Empty<CacheJob>();
        Semaphore = new SemaphoreSlim(1);
    }
}
