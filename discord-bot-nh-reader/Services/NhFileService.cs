using System;
using System.IO;

namespace discord_bot_nh_reader.Services;

public interface INhFileService
{
    Stream? GetReadStream(string path);
    DateTime GetFileCreationTimeUtc(string path);
    Stream CreateWriteStream(string path);
    Stream SetWriteStreamDone(string path, Stream createStream);
}

public class NhFileService : INhFileService
{
    void CreateDir(string path)
    {
        string? dir = Path.GetDirectoryName(path);

        if (dir != null)
        {
            Directory.CreateDirectory(dir);
        }
    }
    
    public Stream? GetReadStream(string path)
    {
        if (!File.Exists(path))
        {
            return null;
        }
        
        CreateDir(path);
        return File.OpenRead(path);
    }

    public DateTime GetFileCreationTimeUtc(string path)
    {
        return new FileInfo(path).CreationTimeUtc;
    }

    public Stream CreateWriteStream(string path)
    {
        CreateDir(path);
        return File.Create(path + ".tmp");
    }

    public Stream SetWriteStreamDone(string path, Stream createStream)
    {
        createStream.Dispose();
        string tmpPath = path + ".tmp";
        File.Move(tmpPath, path, true);
        return File.OpenRead(path);
    }
}
