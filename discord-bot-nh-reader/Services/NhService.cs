using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Json.Serialization.Metadata;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using discord_bot_nh_reader.Services.Api;

namespace discord_bot_nh_reader.Services;

public record NhBook(
    string Id,
    string Title,
    string[] Tags,
    string[] Authors,
    int FavoriteCount,
    DateTime ReleaseDate,
    string SiteUrl,
    string CoverImageUrl,
    string[] PageImageUrls);

public record NhListResult(
    NhBook[] Books,
    int BookCountPerPage);

public interface INhService
{
    Task<NhBook?> GetBookAsync(
        string id,
        CancellationToken cancellationToken);
    Task<NhListResult> GetRelatedBooksAsync(
        string id,
        CancellationToken cancellationToken);
    Task<NhListResult> ListLatestAsync(
        int page,
        CancellationToken cancellationToken);
    Task<NhListResult> ListPopularAsync(
        CancellationToken cancellationToken);
    Task<NhListResult> SearchPopularAsync(
        string query,
        int page,
        CancellationToken cancellationToken);
    Task<NhListResult> SearchLatestAsync(
        string query,
        int page,
        CancellationToken cancellationToken);
    Task<Stream> DownloadCoverImageAsync(
        NhBook book,
        CancellationToken cancellationToken);
    bool IsPageImageCacheAvailable(NhBook book, int page);
    Task<Stream> DownloadPageImageAsync(
        NhBook book,
        int page,
        CancellationToken cancellationToken);
}

public class NhService : INhService
{
    const string API = "https://nhentai.net";
    const string API_T = "https://t.nhentai.net";
    const string API_I = "https://i.nhentai.net";

    readonly IBookRepository m_bookRepository;
    readonly IConfigurationService m_configurationService;
    readonly INhFileService m_fileService;
    readonly INhHomeParserService m_homeParserService;
    readonly IHttpClientFactory m_clientFactory;

    public NhService(
        IBookRepository bookRepository,
        IConfigurationService configurationService,
        INhFileService fileService,
        INhHomeParserService homeParserService,
        IHttpClientFactory clientFactory)
    {
        m_bookRepository = bookRepository;
        m_configurationService = configurationService;
        m_fileService = fileService;
        m_homeParserService = homeParserService;
        m_clientFactory = clientFactory;
    }

    string GetPath(string path, params string[] parameters)
    {
        return API + string.Format(path, parameters);
    }

    async Task<T?> Get<T>(string url, JsonTypeInfo<T> tinfo, CancellationToken cancellationToken)
    {
        try
        {
            using HttpClient client = m_clientFactory.CreateClient("NH");
            HttpResponseMessage response = await client.GetAsync(
                url,
                cancellationToken);

            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return default;
                }

                throw new Exception("NH api error: " + response.StatusCode);
            }

            string responseData = await response.Content.ReadAsStringAsync(
                cancellationToken);
            return JsonSerializer.Deserialize(
                responseData,
                tinfo);
        }
        catch (OperationCanceledException)
        {
            if (!cancellationToken.IsCancellationRequested)
            {
                throw new TimeoutException();
            }

            throw;
        }
    }

    NhBook? GetNhBook(ApiBook? book)
    {
        if (book == null ||
            book.MediaId == null ||
            book.Title == null ||
            book.Title.Title == null ||
            book.Tags == null ||
            book.Tags.Length == 0 ||
            book.Images == null ||
            book.Images.Cover == null ||
            book.Images.Pages == null)
        {
            return null;
        }

        string[] tags = book.Tags
            .Where(x =>
                !string.IsNullOrEmpty(x.Name) &&
                x.Type != "artist")
            .Select(x => x.Name!)
            .ToArray();
        string[] authors = book.Tags
            .Where(x =>
                !string.IsNullOrEmpty(x.Name) &&
                x.Type == "artist")
            .Select(x => x.Name!)
            .ToArray();
        string bookUrl = string.Format(
            "{0}/g/{1}/",
            API,
            book.Id);
        string coverUrl = string.Format(
            "{0}/galleries/{1}/cover.{2}",
            API_T,
            book.MediaId,
            book.Images.Cover.GetExtension());
        int pageCounter = 1;
        string[] pageUrls = book.Images.Pages
            .Select(x =>
                string.Format(
                    "{0}/galleries/{1}/{2}.{3}",
                    API_I,
                    book.MediaId,
                    pageCounter++,
                    x.GetExtension()))
            .ToArray();
        return new NhBook(
            book.Id.ToString(),
            book.Title.Title,
            tags,
            authors,
            book.FavoriteCount,
            book.UploadDate,
            bookUrl,
            coverUrl,
            pageUrls);
    }
    
    public async Task<NhBook?> GetBookAsync(string id, CancellationToken cancellationToken)
    {
        NhBook? book = await m_bookRepository.GetAsync(id);

        if (book != null)
        {
            return book;
        }
        
        ApiBook? apiBook = await Get(
            GetPath("/api/gallery/{0}", id),
            NhServiceContext.Default.ApiBook,
            cancellationToken);
        book = GetNhBook(apiBook);

        if (book == null)
        {
            return null;
        }

        await m_bookRepository.AddAsync(book);
        return book;
    }

    public async Task<NhListResult> GetRelatedBooksAsync(string id, CancellationToken cancellationToken)
    {
        ApiSearch? search = await Get(
            GetPath("/api/gallery/{0}/related", id),
            NhServiceContext.Default.ApiSearch,
            cancellationToken);

        if (search == null ||
            search.Books == null ||
            search.Books.Length == 0)
        {
            return new NhListResult(
                Array.Empty<NhBook>(),
                0);
        }

        NhBook[] books = search.Books
            .Select(GetNhBook)
            .Where(x => x != null)
            .ToArray()!;
        return new NhListResult(
            books,
            books.Length);
    }

    public async Task<NhListResult> ListLatestAsync(int page, CancellationToken cancellationToken)
    {
        ApiSearch? search = await Get(
            GetPath("/api/galleries/all?page={0}", page.ToString()),
            NhServiceContext.Default.ApiSearch,
            cancellationToken);

        if (search == null ||
            search.Books == null ||
            search.Books.Length == 0)
        {
            return new NhListResult(
                Array.Empty<NhBook>(),
                0);
        }

        NhBook[] books = search.Books
            .Select(GetNhBook)
            .Where(x => x != null)
            .ToArray()!;
        return new NhListResult(
            books,
            search.BookCountPerPage);
    }

    async Task<string> GetPopularHtmlAsync(CancellationToken cancellationToken)
    {
        string path = Path.Combine(
            m_configurationService.CacheDir,
            "popular.html");
        Stream? cacheStream = m_fileService.GetReadStream(path);

        if (cacheStream != null)
        {
            DateTime cacheTime = m_fileService.GetFileCreationTimeUtc(path);
            TimeSpan cacheAge = DateTime.UtcNow - cacheTime;

            if (cacheAge > TimeSpan.FromHours(1))
            {
                cacheStream.Dispose();
                cacheStream = null;
            }
        }

        if (cacheStream == null)
        {
            cacheStream = m_fileService.CreateWriteStream(path);
            
            try
            {
                await DownloadAsync(
                    API,
                    cacheStream,
                    cancellationToken);
            }
            catch
            {
                cacheStream.Dispose();
                throw;
            }

            cacheStream = m_fileService.SetWriteStreamDone(path, cacheStream);
        }

        using StreamReader reader = new StreamReader(cacheStream);
        return await reader.ReadToEndAsync(cancellationToken);
    }

    public async Task<NhListResult> ListPopularAsync(CancellationToken cancellationToken)
    {
        string htmlString = await GetPopularHtmlAsync(cancellationToken);
        string[] ids = await m_homeParserService.GetBookIdsAsync(htmlString);
        List<NhBook> books = new List<NhBook>();

        foreach (string id in ids)
        {
            NhBook? book = await GetBookAsync(id, cancellationToken);

            if (book == null)
            {
                continue;
            }

            books.Add(book);
        }

        return new NhListResult(
            books.ToArray(),
            books.Count);
    }

    string GetQuery(string query)
    {
        if (query.Contains(','))
        {
            var queries = query.Split(
                    ',',
                    StringSplitOptions.RemoveEmptyEntries |
                    StringSplitOptions.TrimEntries)
                .Select(x =>
                {
                    if (x.Contains('-'))
                    {
                        return x;
                    }

                    return $"+{x}";
                });
            query = string.Join(" ", queries);
        }
        
        if (!query.Contains('"') &&
            !query.Contains('+') &&
            !query.Contains('-'))
        {
            query = $"\"{query}\"";
        }

        return HttpUtility.UrlEncode(query);
    }

    public async Task<NhListResult> SearchPopularAsync(string query, int page, CancellationToken cancellationToken)
    {
        string queryApi = GetPath(
            "/api/galleries/search?query={0}&page={1}&sort=popular",
            GetQuery(query),
            page.ToString());
        ApiSearch? search = await Get(
            queryApi,
            NhServiceContext.Default.ApiSearch,
            cancellationToken);

        if (search == null ||
            search.Books == null ||
            search.Books.Length == 0)
        {
            return new NhListResult(
                Array.Empty<NhBook>(),
                0);
        }

        NhBook[] books = search.Books
            .Select(GetNhBook)
            .Where(x => x != null)
            .ToArray()!;
        return new NhListResult(
            books,
            search.BookCountPerPage);
    }

    public async Task<NhListResult> SearchLatestAsync(string query, int page, CancellationToken cancellationToken)
    {
        string queryApi = GetPath(
            "/api/galleries/search?query={0}&page={1}&sort=date",
            GetQuery(query),
            page.ToString());
        ApiSearch? search = await Get(
            queryApi,
            NhServiceContext.Default.ApiSearch,
            cancellationToken);

        if (search == null ||
            search.Books == null ||
            search.Books.Length == 0)
        {
            return new NhListResult(
                Array.Empty<NhBook>(),
                0);
        }

        NhBook[] books = search.Books
            .Select(GetNhBook)
            .Where(x => x != null)
            .ToArray()!;
        return new NhListResult(
            books,
            books.Length);
    }

    async Task DownloadAsync(string url, Stream saveStream, CancellationToken cancellationToken)
    {
        try
        {
            using HttpClient client = m_clientFactory.CreateClient("NH");
            using Stream downloadStream = await client.GetStreamAsync(
                url,
                cancellationToken);
            await downloadStream.CopyToAsync(
                saveStream,
                cancellationToken);
        }
        catch (OperationCanceledException)
        {
            if (!cancellationToken.IsCancellationRequested)
            {
                throw new TimeoutException();
            }

            throw;
        }
    }

    public async Task<Stream> DownloadCoverImageAsync(NhBook book, CancellationToken cancellationToken)
    {
        string path = Path.Combine(
            m_configurationService.CacheDir,
            book.Id,
            Path.GetFileName(book.CoverImageUrl));
        Stream? cacheStream = m_fileService.GetReadStream(path);

        if (cacheStream != null)
        {
            return cacheStream;
        }

        Stream downloadStream = m_fileService.CreateWriteStream(path);

        try
        {
            await DownloadAsync(
                book.CoverImageUrl,
                downloadStream,
                cancellationToken);
            return m_fileService.SetWriteStreamDone(path, downloadStream);
        }
        catch
        {
            downloadStream.Dispose();
            throw;
        }
    }

    public bool IsPageImageCacheAvailable(NhBook book, int page)
    {
        string path = Path.Combine(
            m_configurationService.CacheDir,
            book.Id,
            Path.GetFileName(book.PageImageUrls[page - 1]));
        Stream? cacheStream = m_fileService.GetReadStream(path);
        cacheStream?.Dispose();

        return cacheStream != null;
    }

    public async Task<Stream> DownloadPageImageAsync(NhBook book, int page, CancellationToken cancellationToken)
    {
        if (page < 1 ||
            page > book.PageImageUrls.Length)
        {
            throw new ArgumentException("Invalid page number");
        }
        
        string path = Path.Combine(
            m_configurationService.CacheDir,
            book.Id,
            Path.GetFileName(book.PageImageUrls[page - 1]));
        Stream? cacheStream = m_fileService.GetReadStream(path);

        if (cacheStream != null)
        {
            return cacheStream;
        }

        Stream downloadStream = m_fileService.CreateWriteStream(path);
        
        try
        {
            await DownloadAsync(
                book.PageImageUrls[page - 1],
                downloadStream,
                cancellationToken);
            return m_fileService.SetWriteStreamDone(path, downloadStream);
        }
        catch
        {
            downloadStream.Dispose();
            throw;
        }
    }
}

[JsonSerializable(typeof(ApiBook))]
[JsonSerializable(typeof(ApiSearch))]
public partial class NhServiceContext : JsonSerializerContext
{
}
