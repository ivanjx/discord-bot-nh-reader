using System;

namespace discord_bot_nh_reader.Services;

public interface IConfigurationService
{
    string Token
    {
        get;
    }

    string Prefix
    {
        get;
    }

    string CacheDir
    {
        get;
    }

    string DataDir
    {
        get;
    }

    string? FlareSolverrEndpoint
    {
        get;
    }
    
    string? SeqEndpoint
    {
        get;
    }

    string? SeqApiKey
    {
        get;
    }
}

public class ConfigurationService : IConfigurationService
{
    public string Token { get; }
    public string Prefix { get; }
    public string CacheDir { get; }
    public string DataDir { get; }
    public string? FlareSolverrEndpoint { get; }
    public string? SeqEndpoint { get; }
    public string? SeqApiKey { get; }

    public ConfigurationService()
    {
        Token = 
            Environment.GetEnvironmentVariable("TOKEN") ??
            throw new Exception("Missing TOKEN");
        Prefix =
            Environment.GetEnvironmentVariable("PREFIX") ??
            throw new Exception("Missing PREFIX");
        DataDir =
            Environment.GetEnvironmentVariable("DATA_DIR") ??
            throw new Exception("Missing DATA_DIR");
        CacheDir =
            Environment.GetEnvironmentVariable("CACHE_DIR") ??
            throw new Exception("Missing CACHE_DIR");
        FlareSolverrEndpoint =
            Environment.GetEnvironmentVariable("FLARESOLVERR_ENDPOINT");
        SeqEndpoint =
            Environment.GetEnvironmentVariable("SEQ_ENDPOINT");
        SeqApiKey =
            Environment.GetEnvironmentVariable("SEQ_API_KEY");
    }
}
