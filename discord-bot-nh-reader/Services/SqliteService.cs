using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace discord_bot_nh_reader.Services;

public interface ISqliteService
{
    SqliteConnection Connection
    {
        get;
    }

    Task InitAsync();
    Task CloseAsync();
}

public class SqliteService : ISqliteService
{
    IConfigurationService m_configurationService;
    SqliteConnection? m_conn;

    public SqliteConnection Connection =>
        m_conn ??
        throw new Exception("Sqlite not initialized"); // Impossible.

    public SqliteService(
        IConfigurationService configurationService)
    {
        m_configurationService = configurationService;
    }

    public async Task InitAsync()
    {
        // Opening db connection.
        Directory.CreateDirectory(
            m_configurationService.DataDir);
        string connStr = string.Format(
            "Data Source={0};",
            Path.Combine(m_configurationService.DataDir, "state.db"));
        m_conn = new SqliteConnection(connStr);
        await m_conn.OpenAsync();
        
        // Initializing db schema.
        string query = @"
CREATE TABLE IF NOT EXISTS books (
    id VARCHAR(16) NOT NULL UNIQUE,
    title TEXT NOT NULL,
    tags TEXT NOT NULL,
    authors TEXT NOT NULL,
    favorite_count INT NOT NULL,
    release_date INT NOT NULL,
    site_url TEXT NOT NULL,
    cover_image_url TEXT NOT NULL,
    page_image_urls TEXT NOT NULL,
    creation_time INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cf_tokens (
    token TEXT NOT NULL UNIQUE
);
";
        using SqliteCommand command = m_conn.CreateCommand();
        command.CommandText = query;
        await command.ExecuteNonQueryAsync();
    }

    public async Task CloseAsync()
    {
        if (m_conn == null)
        {
            return;
        }
        
        await m_conn.CloseAsync();
        m_conn = null;
    }
}
