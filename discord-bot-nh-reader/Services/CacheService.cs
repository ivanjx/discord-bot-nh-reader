using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace discord_bot_nh_reader.Services;

public interface ICacheService
{
    void AddCacheJob(
        NhBook book,
        int fromPage);
    Task DoCacheJobAsyc(
        CancellationToken cancellationToken);
}

public class CacheService : ICacheService
{
    CacheState m_state;
    INhService m_nhService;
    ILogger m_logger;

    public CacheService(
        CacheState state,
        INhService nhService,
        ILogger logger)
    {
        m_state = state;
        m_nhService = nhService;
        m_logger = logger;
    }

    async Task AddCacheJobAsync(NhBook book, int fromPage)
    {
        ILogger logger = m_logger
            .ForContext("book", book)
            .ForContext("fromPage", fromPage);
        bool success = await m_state.Semaphore.WaitAsync(
            TimeSpan.FromSeconds(5));

        if (!success)
        {
            logger.Error("Unable to enter semaphore");
            return;
        }
        
        int downloadCount = Math.Min(
            5,
            book.PageImageUrls.Length - fromPage);
        int downloadedCount = 0;

        for (int i = 0; i < downloadCount; ++i)
        {
            if (m_nhService.IsPageImageCacheAvailable(book, fromPage + i))
            {
                ++downloadedCount;
            }
        }

        if (downloadedCount >= 2)
        {
            m_state.Semaphore.Release();
            return;
        }

        for (int i = 0; i < downloadCount; ++i)
        {
            m_state.Jobs = m_state.Jobs
                .Append(new CacheJob(book, fromPage + i))
                .ToArray();
            logger
                .ForContext("page", fromPage + i)
                .Information("Added page cache job");
        }
        
        m_state.Semaphore.Release();
    }
    
    public void AddCacheJob(NhBook book, int fromPage)
    {
        _ = AddCacheJobAsync(book, fromPage);
    }

    public async Task DoCacheJobAsyc(CancellationToken cancellationToken)
    {
        CacheJob[] jobs = m_state.Jobs;

        if (jobs.Length == 0)
        {
            return;
        }

        foreach (CacheJob job in jobs)
        {
            ILogger logger = m_logger
                .ForContext("book", job.Book)
                .ForContext("page", job.Page);
            logger.Information("Downloading page for cache");

            try
            {
                using Stream _ = await m_nhService.DownloadPageImageAsync(
                    job.Book,
                    job.Page,
                    cancellationToken);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error downloading page for cache");
            }

            bool success = await m_state.Semaphore.WaitAsync(
                TimeSpan.FromSeconds(5),
                cancellationToken);

            if (!success)
            {
                logger.Error("Unable to enter semaphore");
                m_state.Jobs = Array.Empty<CacheJob>();
                return;
            }
            
            m_state.Jobs = m_state.Jobs
                .Where(x =>
                    x.Book.Id != job.Book.Id &&
                    x.Page != job.Page)
                .ToArray();
            m_state.Semaphore.Release();
        }
    }
}
