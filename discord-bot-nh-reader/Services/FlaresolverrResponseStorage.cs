using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using FlareSolverrSharp;
using FlareSolverrSharp.Types;
using Microsoft.Data.Sqlite;

namespace discord_bot_nh_reader.Services;

public class FlaresolverrResponseStorage : IFlaresolverrResponseStorage
{
    ISqliteService m_sqliteService;

    public FlaresolverrResponseStorage(
        ISqliteService sqliteService)
    {
        m_sqliteService = sqliteService;
    }

    // This is nullable.
    public async Task<FlareSolverrResponse?> LoadAsync()
    {
        using SqliteCommand command = m_sqliteService.Connection.CreateCommand();
        command.CommandText = @"
            SELECT token
            FROM cf_tokens
            LIMIT 1";
        command.Prepare();
        using SqliteDataReader reader = await command.ExecuteReaderAsync();

        if (!await reader.ReadAsync())
        {
            return null;
        }

        string raw = reader.GetString(0);
        return JsonSerializer.Deserialize(
            raw,
            FlaresolverrResponseStorageContext.Default.FlareSolverrResponse);
    }

    public async Task SaveAsync(FlareSolverrResponse result)
    {
        string raw = JsonSerializer.Serialize(
            result,
            FlaresolverrResponseStorageContext.Default.FlareSolverrResponse);
        using SqliteCommand clearCommand = m_sqliteService.Connection.CreateCommand();
        clearCommand.CommandText = @"DELETE FROM cf_tokens";
        clearCommand.Prepare();
        await clearCommand.ExecuteNonQueryAsync();

        using SqliteCommand insertCommand = m_sqliteService.Connection.CreateCommand();
        insertCommand.CommandText = @"
            INSERT INTO cf_tokens (token)
            VALUES($token)";
        insertCommand.Parameters.AddWithValue("$token", raw);
        insertCommand.Prepare();
        await insertCommand.ExecuteNonQueryAsync();
    }
}

[JsonSerializable(typeof(FlareSolverrResponse))]
public partial class FlaresolverrResponseStorageContext : JsonSerializerContext
{
}
