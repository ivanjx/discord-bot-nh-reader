﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using discord_bot_nh_reader.Responders;
using discord_bot_nh_reader.Services;
using FlareSolverrSharp;
using Microsoft.Extensions.DependencyInjection;
using Remora.Discord.API.Abstractions.Gateway.Commands;
using Remora.Discord.Gateway;
using Remora.Discord.Gateway.Extensions;
using Remora.Results;
using Serilog;
using Serilog.Events;
using CacheJob = discord_bot_nh_reader.Jobs.CacheJob;

namespace discord_bot_nh_reader;

public class Program
{
    public static async Task Main()
    {
        IConfigurationService configurationService = new ConfigurationService();
        LoggerConfiguration loggerConfig = new LoggerConfiguration()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
            .MinimumLevel.Override("System", LogEventLevel.Error)
            .Enrich.FromLogContext();

        if (configurationService.SeqEndpoint != null &&
            configurationService.SeqApiKey != null)
        {
            loggerConfig = loggerConfig.WriteTo.Seq(
                configurationService.SeqEndpoint,
                apiKey: configurationService.SeqApiKey);
        }
        else
        {
            loggerConfig = loggerConfig.WriteTo.Console(
                outputTemplate: "[{Timestamp:u} {Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}");
        }

        Log.Logger = loggerConfig.CreateLogger();
        ILogger logger = Log.ForContext<Program>();

        try
        {
            logger.Information("Initializing database");
            ISqliteService sqliteService = new SqliteService(
                configurationService);
            await sqliteService.InitAsync();

            logger.Information("Initializing services");
            IServiceCollection serviceCollection = new ServiceCollection()
                .AddSingleton(configurationService)
                .AddSingleton(Log.Logger)
                .AddSingleton(sqliteService)
                .AddSingleton<IBookRepository, BookRepository>()
                .AddSingleton<IChannelRepository, ChannelRepository>()
                .AddSingleton<INhFileService, NhFileService>()
                .AddSingleton<INhHomeParserService, NhHomeParserService>()
                .AddSingleton<INhService, NhService>()
                .AddSingleton<BotState>()
                .AddSingleton<CacheState>()
                .AddSingleton<ICacheService, CacheService>()
                .AddSingleton<CacheJob>()
                .AddResponder<ReadyResponder>()
                .AddResponder<HelpResponder>()
                .AddResponder<SearchResponder>()
                .AddResponder<EmojiResponder>()
                .Configure<DiscordGatewayClientOptions>(x =>
                    x.Intents |=
                        GatewayIntents.MessageContents |
                        GatewayIntents.GuildMessages |
                        GatewayIntents.GuildMessageReactions)
                .AddDiscordGateway(_ => configurationService.Token)
                .AddHttpClient();
            
            if (string.IsNullOrEmpty(configurationService.FlareSolverrEndpoint))
            {
                serviceCollection
                    .AddHttpClient("NH")
                    .ConfigureHttpClient(client =>
                        client.Timeout = TimeSpan.FromSeconds(10));
            }
            else
            {
                TimeSpan flaresolverrTimeout = TimeSpan.FromMinutes(3);

                serviceCollection
                    .AddHttpClient("NH")
                    .ConfigurePrimaryHttpMessageHandler(_ =>
                    {
                        ClearanceHandler handler = new ClearanceHandler(
                            configurationService.FlareSolverrEndpoint,
                            new FlaresolverrResponseStorage(sqliteService));
                        handler.MaxTimeout = (int)flaresolverrTimeout.TotalMilliseconds;
                        return handler;
                    })
                    .ConfigureHttpClient(client =>
                        client.Timeout = flaresolverrTimeout);
            }
            
            ServiceProvider services = serviceCollection.BuildServiceProvider();
            
            logger.Information("Registering SIGINT handler");
            CancellationTokenSource cts = new CancellationTokenSource();
            Console.CancelKeyPress += (_, e) =>
            {
                logger
                    .ForContext("reason", nameof(Console.CancelKeyPress))
                    .Error("Exit requested");
                e.Cancel = true;
                
                try
                {
                    cts.Cancel();
                }
                catch { }
            };

            logger.Information("Starting jobs");
            CacheJob cacheJob = services.GetRequiredService<CacheJob>();
            _ = cacheJob.StartAsync(cts.Token);
            
            logger.Information("Connecting to discord");
            DiscordGatewayClient client = services.GetRequiredService<DiscordGatewayClient>();
            Result botResult = await client.RunAsync(cts.Token);

            logger.Information("Closing database");
            await sqliteService.CloseAsync();
            
            logger
                .ForContext("errorMessage", botResult.Error?.Message)
                .Information("Execution completed");
        }
        catch (Exception ex)
        {
            logger.Fatal(ex, "Fatal error");
        }
    }
}
