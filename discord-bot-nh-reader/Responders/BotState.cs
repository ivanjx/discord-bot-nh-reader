using System;
using Remora.Rest.Core;

namespace discord_bot_nh_reader.Responders;

public class BotState
{
    public Snowflake BotId
    {
        get;
        set;
    }
}
