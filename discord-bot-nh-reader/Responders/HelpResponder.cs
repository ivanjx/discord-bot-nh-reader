using System;
using System.Threading;
using System.Threading.Tasks;
using discord_bot_nh_reader.Services;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Discord.API.Abstractions.Rest;
using Remora.Discord.API.Objects;
using Remora.Discord.Gateway.Responders;
using Remora.Results;
using Serilog;

namespace discord_bot_nh_reader.Responders;

public class HelpResponder : IResponder<IMessageCreate>
{
    readonly IConfigurationService m_configurationService;
    readonly IDiscordRestChannelAPI m_channelApi;
    readonly ILogger m_logger;

    public HelpResponder(
        IConfigurationService configurationService,
        IDiscordRestChannelAPI channelApi,
        ILogger logger)
    {
        m_configurationService = configurationService;
        m_channelApi = channelApi;
        m_logger = logger;
    }
    
    public async Task<Result> RespondAsync(IMessageCreate message, CancellationToken ct = default)
    {
        if (!message.IsValid("help", m_configurationService) &&
            !message.IsValid("h", m_configurationService))
        {
            return Result.FromSuccess();
        }

        ILogger logger = m_logger.GetLoggerContext(message);
        logger.Information("Sending help message");
        Result<IMessage> result = await m_channelApi.CreateMessageAsync(
            message.ChannelID,
            messageReference: new MessageReference(message.ID),
            content: string.Format(HELP_MSG, m_configurationService.Prefix));

        if (!result.IsSuccess)
        {
            logger.Error(
                new Exception(result.Error.Message),
                "Error sending help message");
        }

        return (Result)result;
    }

    const string HELP_MSG =
@"**Available Commands**
- `{0}search (id)` Get specific manga using its ID.
- `{0}search query` Get currently popular manga (only 5).
- `{0}search query (search query)` Search manga and sort them by its popularity.
- `{0}search latest (search query)` Search manga and sort them by its release date (newest).
- `{0}read (id)` Start reading specific manga using its ID.

You can also use command initials for quicker typing. Examples:
- `{0}s (id)` Get specific manga using its ID.
- `{0}s q` Get currently popular manga (only 5).
- `{0}s q (search query)` Search manga and sort them by its popularity.
- etc...

Enjoy!";
}
