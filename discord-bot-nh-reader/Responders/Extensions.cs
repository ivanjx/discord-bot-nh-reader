using System;
using System.Text.RegularExpressions;
using discord_bot_nh_reader.Services;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Rest.Core;
using Serilog;

namespace discord_bot_nh_reader.Responders;

public static partial class Extensions
{
    public static bool IsNumeric(this string str)
    {
        return IsNumericRegex().IsMatch(str);
    }

    [GeneratedRegex("^[0-9]+$")]
    private static partial Regex IsNumericRegex();
    
    public static bool IsValid(this IMessage message, string commandPrefix, IConfigurationService configurationService)
    {
        if (message.Author.IsBot.HasValue &&
            message.Author.IsBot.Value)
        {
            return false;
        }
        
        if (!message.Content.StartsWith(configurationService.Prefix + commandPrefix))
        {
            return false;
        }

        return true;
    }
    
    public static bool IsValid(this IMessage message, string commandPrefix, IConfigurationService configurationService, out string remainder)
    {
        if (message.Author.IsBot.HasValue &&
            message.Author.IsBot.Value)
        {
            remainder = "";
            return false;
        }

        string prefix = configurationService.Prefix + commandPrefix;
        int index = message.Content.IndexOf(
            prefix,
            StringComparison.InvariantCulture);
        
        if (index == -1)
        {
            remainder = "";
            return false;
        }

        remainder = message.Content
            .Substring(index + prefix.Length)
            .Trim();
        return true;
    }

    public static ILogger GetLoggerContext(this ILogger logger, IMessageCreate message)
    {
        return logger
            .ForContext("guild", message.GuildID)
            .ForContext("channel", message.ChannelID)
            .ForContext("user", message.Author.ID)
            .ForContext("content", message.Content);
    }

    public static ILogger GetLoggerContext(this ILogger logger, IMessageReactionAdd reaction)
    {
        Snowflake userId = default;

        if (reaction.Member.HasValue &&
            reaction.Member.Value.User.HasValue)
        {
            userId = reaction.Member.Value.User.Value.ID;
        }

        return logger
            .ForContext("guild", reaction.GuildID)
            .ForContext("channel", reaction.ChannelID)
            .ForContext("user", userId)
            .ForContext("emoji", reaction.Emoji);
    }

    public static string GetUnknownCommandMessage(this IConfigurationService configurationService)
    {
        return string.Format(
            "Unkown command. Try `{0}help` for available commands",
            configurationService.Prefix);
    }
}
