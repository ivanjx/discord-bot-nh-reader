using System;
using System.Threading;
using System.Threading.Tasks;
using discord_bot_nh_reader.Services;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Discord.API.Gateway.Commands;
using Remora.Discord.API.Objects;
using Remora.Discord.Gateway;
using Remora.Discord.Gateway.Responders;
using Remora.Results;
using Serilog;

namespace discord_bot_nh_reader.Responders;

public class ReadyResponder : IResponder<IReady>
{
    readonly BotState m_state;
    readonly IConfigurationService m_configurationService;
    readonly DiscordGatewayClient m_client;
    readonly ILogger m_logger;

    public ReadyResponder(
        BotState state,
        IConfigurationService configurationService,
        DiscordGatewayClient client,
        ILogger logger)
    {
        m_state = state;
        m_configurationService = configurationService;
        m_client = client;
        m_logger = logger.ForContext<ReadyResponder>();
    }
    
    public Task<Result> RespondAsync(IReady gatewayEvent, CancellationToken ct = new CancellationToken())
    {
        m_logger.Information("Connected to discord");
        m_state.BotId = gatewayEvent.User.ID;

        m_logger.Information("Setting bot status");
        UpdatePresence command = new UpdatePresence(
            UserStatus.Online,
            false,
            null,
            new[]
            {
                new Activity(
                    string.Format(
                        "for your commands! Use {0}help for details.",
                        m_configurationService.Prefix),
                    ActivityType.Watching)
            });
        m_client.SubmitCommand(command);
        return Task.FromResult(Result.FromSuccess());
    }
}
