using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using discord_bot_nh_reader.Services;
using OneOf;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Discord.API.Abstractions.Rest;
using Remora.Discord.API.Abstractions.Results;
using Remora.Discord.API.Objects;
using Remora.Discord.Gateway.Responders;
using Remora.Rest.Results;
using Remora.Results;
using Serilog;

namespace discord_bot_nh_reader.Responders;

public class SearchResponder : IResponder<IMessageCreate>
{
    const int MAX_BOOK_REPLY_COUNT = 5;

    readonly IChannelRepository m_channelRepository;
    readonly IConfigurationService m_configurationService;
    readonly INhService m_nhService;
    readonly IDiscordRestChannelAPI m_channelApi;
    readonly ILogger m_logger;

    public SearchResponder(
        IChannelRepository channelRepository,
        IConfigurationService configurationService,
        INhService nhService,
        IDiscordRestChannelAPI channelApi,
        ILogger logger)
    {
        m_channelRepository = channelRepository;
        m_configurationService = configurationService;
        m_nhService = nhService;
        m_channelApi = channelApi;
        m_logger = logger.ForContext<SearchResponder>();
    }

    async Task<Result<IMessage>> CreateQueryStatusMessageAsync(IMessageCreate message, string? query, bool isPopular)
    {
        EmbedField[] fields;

        if (query == null)
        {
            // Popular or latest books.
            fields = new[]
            {
                new EmbedField(
                    "Type",
                    isPopular ? "Popular" : "Latest")
            };
        }
        else if (query.StartsWith("#"))
        {
            // Book id search.
            fields = new[]
            {
                new EmbedField("Id", query)
            };
        }
        else
        {
            // Popular or latest books with query.
            fields = new[]
            {
                new EmbedField(
                    "Sort",
                    isPopular ? "Popular" : "Latest"),
                new EmbedField("Query", query)
            };
        }

        Embed rootEmbed = new Embed(
            Colour: Color.Orange,
            Title: "Processing your request",
            Fields: fields);
        Result<IMessage> rootMsgResult = await m_channelApi.CreateMessageAsync(
            message.ChannelID,
            messageReference: new MessageReference(message.ID),
            embeds: new[] { rootEmbed });
        
        if (!rootMsgResult.IsSuccess)
        {
            return rootMsgResult;
        }

        await m_channelApi.CreateReactionAsync(
            rootMsgResult.Entity.ChannelID,
            rootMsgResult.Entity.ID,
            "🛑");
        
        return rootMsgResult;
    }

    async Task SetQueryStatusMessageErrorAsync(IMessage queryMsg, string errorStr)
    {
        Embed embed = new Embed(
            Colour: Color.Red,
            Title: errorStr,
            Fields: queryMsg.Embeds[0].Fields);
        await m_channelApi.EditMessageAsync(
            queryMsg.ChannelID,
            queryMsg.ID,
            embeds: new[] { embed });
        await m_channelApi.DeleteAllReactionsAsync(
            queryMsg.ChannelID,
            queryMsg.ID);
    }

    async Task SetQueryStatusMessageDoneAsync(IMessage queryMsg, int bookCount, bool isHomePage)
    {
        await m_channelApi.DeleteAllReactionsAsync(
            queryMsg.ChannelID,
            queryMsg.ID);
        
        IEmbedField[] fields = queryMsg.Embeds[0].Fields.Value.ToArray();

        if (bookCount == MAX_BOOK_REPLY_COUNT &&
            !isHomePage)
        {
            await m_channelApi.CreateReactionAsync(
                queryMsg.ChannelID,
                queryMsg.ID,
                "➡️");
        }

        Embed embed = new Embed(
            Colour: Color.Green,
            Title: "Processing done",
            Description: string.Format(
                "Query returned {0} {1}",
                bookCount,
                bookCount == 1 ? "book" : "books"),
            Fields: fields);
        await m_channelApi.EditMessageAsync(
            queryMsg.ChannelID,
            queryMsg.ID,
            embeds: new[] { embed });
    }

    async Task<Result> SendBooksReplyAsync(IMessage rootMsg, NhBook[] books, ILogger logger)
    {
        int counter = 1;

        foreach (NhBook book in books)
        {
            ILogger bookLogger = logger.ForContext("book", book);
            Stream? thumbnailStream = null;
            Stream? firstPageStream = null;

            try
            {
                bookLogger.Information("Downloading book cover image");
                thumbnailStream = await m_nhService.DownloadCoverImageAsync(book, default);
                string thumbnailAttachmentName = Path.GetFileName(book.CoverImageUrl);
                FileData thumbnailAttachment = new FileData(
                    thumbnailAttachmentName,
                    thumbnailStream);
                
                bookLogger.Information("Downloading book first page image");
                firstPageStream = await m_nhService.DownloadPageImageAsync(book, 1, default);
                string firstPageAttachmentName = Path.GetFileName(book.PageImageUrls[0]);
                FileData firstPageAttachment = new FileData(
                    firstPageAttachmentName,
                    firstPageStream);
                
                bookLogger.Information("Creating book embed message");
                string[] tags = book.Tags
                    .Select(x =>
                    {
                        if (x == "english")
                        {
                            return x + " 🇬🇧";
                        }
                        
                        if (x == "japanese")
                        {
                            return x + " 🇯🇵";
                        }
                        
                        if (x == "chinese")
                        {
                            return x + " 🇨🇳";
                        }

                        if (x == "netorare" ||
                            x == "guro" ||
                            x == "farting")
                        {
                            return x + " ⚠️";
                        }

                        if (x == "lolicon" ||
                            x == "loli")
                        {
                            return x + " 😭";
                        }

                        return x;
                    })
                    .ToArray();
                Embed bookEmbed = new Embed(
                    Colour: Color.Black,
                    Title: string.Format(
                        "{0}. {1}",
                        counter++,
                        book.Title),
                    Url: book.SiteUrl,
                    Thumbnail: new EmbedThumbnail("attachment://" + thumbnailAttachmentName),
                    Image: new EmbedImage("attachment://" + firstPageAttachmentName),
                    Fields: new[]
                    {
                        new EmbedField("Id", book.Id),
                        new EmbedField("Author", string.Join(", ", book.Authors)),
                        new EmbedField("Tags", string.Join(", ", tags)),
                        new EmbedField("Release Date", book.ReleaseDate.ToString("yyyy-MM-dd")),
                        new EmbedField("Favorite Count", book.FavoriteCount.ToString()),
                        new EmbedField("Page", string.Format("1/{0}", book.PageImageUrls.Length))
                    });
                Result<IMessage> bookMsgResult = await m_channelApi.CreateMessageAsync(
                    rootMsg.ChannelID,
                    messageReference: new MessageReference(rootMsg.ID),
                    embeds: new[] { bookEmbed },
                    attachments: new[]
                    {
                        OneOf<FileData, IPartialAttachment>.FromT0(thumbnailAttachment),
                        OneOf<FileData, IPartialAttachment>.FromT0(firstPageAttachment)
                    });
                
                if (!bookMsgResult.IsSuccess)
                {
                    bookLogger.Error(
                        new Exception(bookMsgResult.Error.Message),
                        "Error sending book embed message");

                    if (bookMsgResult.Error is not RestResultError<RestError> rer)
                    {
                        return (Result)bookMsgResult;
                    }

                    if (rer.Error.Code != DiscordError.FileUploadExceedsMaximumSize &&
                        rer.Error.Code != DiscordError.RequestEntityTooLarge)
                    {
                        return (Result)bookMsgResult;
                    }

                    bookLogger.Information("Retrying sending book images via link");
                    bookEmbed = bookEmbed with
                    {
                        Thumbnail = new EmbedThumbnail(book.CoverImageUrl),
                        Image = new EmbedImage(book.PageImageUrls[0])
                    };
                    bookMsgResult = await m_channelApi.CreateMessageAsync(
                        rootMsg.ChannelID,
                        messageReference: new MessageReference(rootMsg.ID),
                        embeds: new[] { bookEmbed });

                    if (!bookMsgResult.IsSuccess)
                    {
                        bookLogger.Error(
                            new Exception(bookMsgResult.Error.Message),
                            "Error sending book embed message");
                        return (Result)bookMsgResult;
                    }
                }

                bookLogger.Information("Creating book reply reactions");
                await m_channelApi.CreateReactionAsync(
                    bookMsgResult.Entity.ChannelID,
                    bookMsgResult.Entity.ID,
                    "⛓️");
                await m_channelApi.CreateReactionAsync(
                    bookMsgResult.Entity.ChannelID,
                    bookMsgResult.Entity.ID,
                    "⏮️");
                await m_channelApi.CreateReactionAsync(
                    bookMsgResult.Entity.ChannelID,
                    bookMsgResult.Entity.ID,
                    "⬅️");
                await m_channelApi.CreateReactionAsync(
                    bookMsgResult.Entity.ChannelID,
                    bookMsgResult.Entity.ID,
                    "➡️");
            }
            catch (Exception ex)
            {
                bookLogger.Error(ex, "Error sending book reply");
                return Result.FromError(new ExceptionError(ex));
            }
            finally
            {
                thumbnailStream?.Dispose();
                firstPageStream?.Dispose();
            }
        }

        return Result.FromSuccess();
    }

    async Task<Result> HandlePopularAsync(IMessageCreate message, ILogger logger, CancellationToken ct)
    {
        logger = logger.ForContext("type", "home");
        logger.Information("Sending query status message");
        Result<IMessage> queryMsgResult = await CreateQueryStatusMessageAsync(
            message,
            null,
            true);
        
        if (!queryMsgResult.IsSuccess)
        {
            logger.Error(
                new Exception(queryMsgResult.Error.Message),
                "Error sending query status message");
            return (Result)queryMsgResult;
        }

        NhListResult nhResult;

        try
        {
            logger.Information("Getting popular books");
            nhResult = await m_nhService.ListPopularAsync(ct);
        }
        catch (TaskCanceledException)
        {
            logger.Error("Operation cancelled");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Operation cancelled");
            return Result.FromSuccess();
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Unable to list popular books");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Unable to retrieve popular book listing at the moment");
            return Result.FromError(new ExceptionError(ex));
        }
        
        if (nhResult.Books.Length == 0)
        {
            logger.Error("No popular books");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "No popular books at the moment");
            return Result.FromSuccess();
        }

        if (nhResult.Books.Length > MAX_BOOK_REPLY_COUNT)
        {
            logger
                .ForContext("count", nhResult.Books.Length)
                .Error("Invalid popular books result count");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Unable to retrieve popular book listing at the moment");
            return Result.FromError(
                new ExceptionError(
                    new Exception("Invalid popular books result count")));
        }
        
        logger.Information("Setting query status message done");
        await SetQueryStatusMessageDoneAsync(
            queryMsgResult.Entity,
            nhResult.Books.Length,
            true);

        logger.Information("Sending books reply");
        return await SendBooksReplyAsync(
            queryMsgResult.Entity,
            nhResult.Books,
            logger);
    }
    
    async Task<Result> HandlePopularQueryAsync(IMessageCreate message, string query, ILogger logger, CancellationToken ct)
    {
        logger = logger
            .ForContext("type", "popular")
            .ForContext("query", query);
        logger.Information("Sending query status message");
        Result<IMessage> queryMsgResult = await CreateQueryStatusMessageAsync(
            message,
            query,
            true);
        
        if (!queryMsgResult.IsSuccess)
        {
            logger.Error(
                new Exception(queryMsgResult.Error.Message),
                "Error creating query status message");
            return (Result)queryMsgResult;
        }

        NhListResult nhResult;

        try
        {
            logger.Information("Searching popular books");
            nhResult = await m_nhService.SearchPopularAsync(
                query,
                1,
                ct);
        }
        catch (TaskCanceledException)
        {
            logger.Error("Operation cancelled");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Operation cancelled");
            return Result.FromSuccess();
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Unable to search popular books");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Unable to search books at the moment");
            return Result.FromError(new ExceptionError(ex));
        }

        NhBook[] books = nhResult.Books
            .Take(MAX_BOOK_REPLY_COUNT)
            .ToArray();

        if (books.Length == 0)
        {
            logger.Error("No result");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "No books with such query");
            return Result.FromSuccess();
        }
        
        logger.Information("Setting query status message done");
        await SetQueryStatusMessageDoneAsync(
            queryMsgResult.Entity,
            books.Length,
            false);

        logger.Information("Sending books reply");
        return await SendBooksReplyAsync(
            queryMsgResult.Entity,
            books,
            logger);
    }

    async Task<Result> HandleLatestAsync(IMessageCreate message, ILogger logger, CancellationToken ct)
    {
        logger = logger.ForContext("type", "latest");
        logger.Information("Sending query status message");
        Result<IMessage> queryMsgResult = await CreateQueryStatusMessageAsync(
            message,
            null,
            false);
        
        if (!queryMsgResult.IsSuccess)
        {
            logger.Error(
                new Exception(queryMsgResult.Error.Message),
                "Error creating query status message");
            return (Result)queryMsgResult;
        }

        NhListResult nhResult;

        try
        {
            logger.Information("Getting latest books");
            nhResult = await m_nhService.ListLatestAsync(1, ct);
        }
        catch (TaskCanceledException)
        {
            logger.Error("Operation cancelled");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Operation cancelled");
            return Result.FromSuccess();
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Unable to get latest books");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Unable to retrieve latest books at the moment");
            return Result.FromError(new ExceptionError(ex));
        }

        NhBook[] books = nhResult.Books
            .Take(MAX_BOOK_REPLY_COUNT)
            .ToArray();

        if (books.Length == 0)
        {
            logger.Error("No latest books");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "No latest books at the moment");
            return Result.FromSuccess();
        }
        
        logger.Information("Setting query status message done");
        await SetQueryStatusMessageDoneAsync(
            queryMsgResult.Entity,
            books.Length,
            false);

        logger.Information("Sending books reply");
        return await SendBooksReplyAsync(
            queryMsgResult.Entity,
            books,
            logger);
    }
    
    async Task<Result> HandleLatestQueryAsync(IMessageCreate message, string query, ILogger logger, CancellationToken ct)
    {
        logger = logger
            .ForContext("type", "latest")
            .ForContext("query", query);
        logger.Information("Sending query status message");
        Result<IMessage> queryMsgResult = await CreateQueryStatusMessageAsync(
            message,
            query,
            false);
        
        if (!queryMsgResult.IsSuccess)
        {
            logger.Error(
                new Exception(queryMsgResult.Error.Message),
                "Error creating query status message");
            return (Result)queryMsgResult;
        }

        NhListResult nhResult;

        try
        {
            logger.Information("Searching latest books");
            nhResult = await m_nhService.SearchLatestAsync(
                query,
                1,
                ct);
        }
        catch (TaskCanceledException)
        {
            logger.Error("Operation cancelled");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Operation cancelled");
            return Result.FromSuccess();
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Unable to search latest books");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Unable to search books at the moment");
            return Result.FromError(new ExceptionError(ex));
        }

        NhBook[] books = nhResult.Books
            .Take(MAX_BOOK_REPLY_COUNT)
            .ToArray();

        if (books.Length == 0)
        {
            logger.Error("No latest books");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "No books with such query");
            return Result.FromSuccess();
        }
        
        logger.Information("Setting query status message done");
        await SetQueryStatusMessageDoneAsync(
            queryMsgResult.Entity,
            books.Length,
            false);

        logger.Information("Sending books reply");
        return await SendBooksReplyAsync(
            queryMsgResult.Entity,
            books,
            logger);
    }
    
    async Task<Result> HandleBookIdAsync(IMessageCreate message, string query, ILogger logger, CancellationToken ct)
    {
        logger = logger.ForContext("type", "id");
        logger.Information("Sending query status message");
        Result<IMessage> queryMsgResult = await CreateQueryStatusMessageAsync(
            message,
            "#" + query,
            false);
        
        if (!queryMsgResult.IsSuccess)
        {
            logger.Error(
                new Exception(queryMsgResult.Error.Message),
                "Error creating query status message");
            return (Result)queryMsgResult;
        }

        NhBook? book;

        try
        {
            logger.Information("Getting book with id");
            book = await m_nhService.GetBookAsync(query, ct);
        }
        catch (TaskCanceledException)
        {
            logger.Error("Operation cancelled");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Operation cancelled");
            return Result.FromSuccess();
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Unable to get book with id");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Unable to get book information at the moment");
            return Result.FromError(new ExceptionError(ex));
        }

        if (book == null)
        {
            logger.Error("No book with id");
            await SetQueryStatusMessageErrorAsync(
                queryMsgResult.Entity,
                "Book not found");
            return Result.FromSuccess();
        }

        logger.Information("Setting query status message done");
        await SetQueryStatusMessageDoneAsync(
            queryMsgResult.Entity,
            1,
            false);
        
        logger.Information("Sending books reply");
        return await SendBooksReplyAsync(
            queryMsgResult.Entity,
            new[] { book },
            logger);
    }

    async Task<Result> RespondNoQueryAsync(IMessageCreate message, ILogger logger)
    {
        Result<IMessage> result = await m_channelApi.CreateMessageAsync(
            message.ChannelID,
            content: "Please provide a search query after the command",
            messageReference: new MessageReference(message.ID));

        if (!result.IsSuccess)
        {
            logger.Error(
                new Exception(result.Error.Message),
                "Error sending invalid query response");
        }

        return (Result)result;
    }
    
    public async Task<Result> RespondAsync(IMessageCreate message, CancellationToken ct = default)
    {
        string subCommand;
        
        if (!message.IsValid("search", m_configurationService, out subCommand) &&
            !message.IsValid("s", m_configurationService, out subCommand))
        {
            return Result.FromSuccess();
        }

        ILogger logger = m_logger.GetLoggerContext(message);
        ct = m_channelRepository.GetCancellationToken(
            message.ChannelID.Value,
            ct);

        try
        {
            if (subCommand.StartsWith("q"))
            {
                int queryIndex;

                if (subCommand.StartsWith("q "))
                {
                    queryIndex = 2;
                }
                else if (subCommand.StartsWith("query "))
                {
                    queryIndex = 6;
                }
                else if (
                    subCommand == "q" ||
                    subCommand == "query")
                {
                    queryIndex = -1;
                }
                else
                {
                    return await RespondNoQueryAsync(
                        message,
                        logger);
                }

                if (queryIndex == -1)
                {
                    return await HandlePopularAsync(
                        message,
                        logger,
                        ct);
                }
                else
                {
                    return await HandlePopularQueryAsync(
                        message,
                        subCommand.Substring(queryIndex),
                        logger,
                        ct);
                }
            }
            else if (subCommand.StartsWith("l"))
            {
                int queryIndex;

                if (subCommand.StartsWith("l "))
                {
                    queryIndex = 2;
                }
                else if (subCommand.StartsWith("latest "))
                {
                    queryIndex = 7;
                }
                else if (
                    subCommand == "l" ||
                    subCommand == "latest")
                {
                    queryIndex = -1;
                }
                else
                {
                    return await RespondNoQueryAsync(
                        message,
                        logger);
                }

                if (queryIndex == -1)
                {
                    return await HandleLatestAsync(
                        message,
                        logger,
                        ct);
                }
                else
                {
                    return await HandleLatestQueryAsync(
                        message,
                        subCommand.Substring(queryIndex),
                        logger,
                        ct);
                }
            }
            else if (subCommand.IsNumeric())
            {
                return await HandleBookIdAsync(
                    message,
                    subCommand,
                    logger,
                    ct);
            }
            else
            {
                logger.Information("Sending unkown command response message");
                Result<IMessage> result = await m_channelApi.CreateMessageAsync(
                    message.ChannelID,
                    content: m_configurationService.GetUnknownCommandMessage(),
                    messageReference: new MessageReference(message.ID),
                    ct: ct);

                if (!result.IsSuccess)
                {
                    logger.Error(
                        new Exception(result.Error.Message),
                        "Error sending unknown command response message");
                }

                return (Result)result;
            }
        }
        finally
        {
            m_channelRepository.DestroyCancellationToken(
                message.ChannelID.Value);
        }
    }
}
