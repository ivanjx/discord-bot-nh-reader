using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using discord_bot_nh_reader.Services;
using OneOf;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Discord.API.Abstractions.Rest;
using Remora.Discord.API.Abstractions.Results;
using Remora.Discord.API.Objects;
using Remora.Discord.Gateway.Responders;
using Remora.Rest.Results;
using Remora.Results;
using Serilog;

namespace discord_bot_nh_reader.Responders;

public partial class EmojiResponder : IResponder<IMessageReactionAdd>
{
    const int MAX_BOOK_REPLY_COUNT = 5;

    BotState m_state;
    IChannelRepository m_channelRepository;
    INhService m_nhService;
    ICacheService m_cacheService;
    IDiscordRestChannelAPI m_channelApi;
    ILogger m_logger;

    public EmojiResponder(
        BotState state,
        IChannelRepository channelRepository,
        INhService nhService,
        ICacheService cacheService,
        IDiscordRestChannelAPI channelApi,
        ILogger logger)
    {
        m_state = state;
        m_channelRepository = channelRepository;
        m_nhService = nhService;
        m_cacheService = cacheService;
        m_channelApi = channelApi;
        m_logger = logger.ForContext<EmojiResponder>();
    }
    
    async Task<Result<IMessage>> CreateRelatedQueryStatusMessageAsync(IMessage message, string bookId)
    {
        EmbedField[] fields = new[]
        {
            new EmbedField(
                "Type",
                "Related"),
            new EmbedField(
                "Book Id",
                bookId)
        };
        Embed rootEmbed = new Embed(
            Colour: Color.Orange,
            Title: "Processing your request",
            Fields: fields);
        Result<IMessage> rootMsgResult = await m_channelApi.CreateMessageAsync(
            message.ChannelID,
            messageReference: new MessageReference(message.ID),
            embeds: new[] { rootEmbed });
        
        if (!rootMsgResult.IsSuccess)
        {
            return rootMsgResult;
        }

        await m_channelApi.CreateReactionAsync(
            rootMsgResult.Entity.ChannelID,
            rootMsgResult.Entity.ID,
            "🛑");
        
        return rootMsgResult;
    }

    async Task SetRelatedQueryStatusMessageDoneAsync(IMessage queryMsg, int bookCount)
    {
        await m_channelApi.DeleteAllReactionsAsync(
            queryMsg.ChannelID,
            queryMsg.ID);

        Embed embed = new Embed(
            Colour: Color.Green,
            Title: "Processing done",
            Description: string.Format(
                "Query returned {0} books",
                bookCount),
            Fields: queryMsg.Embeds[0].Fields);
        await m_channelApi.EditMessageAsync(
            queryMsg.ChannelID,
            queryMsg.ID,
            embeds: new[] { embed });
    }

    async Task SetQueryStatusMessageProcessingAsync(IMessage queryMsg)
    {
        await m_channelApi.DeleteAllReactionsAsync(
            queryMsg.ChannelID,
            queryMsg.ID);
        Embed embed = new Embed(
            Colour: Color.Orange,
            Title: "Getting more search results",
            Fields: queryMsg.Embeds[0].Fields);
        await m_channelApi.EditMessageAsync(
            queryMsg.ChannelID,
            queryMsg.ID,
            embeds: new[] { embed });
        await m_channelApi.CreateReactionAsync(
            queryMsg.ChannelID,
            queryMsg.ID,
            "🛑");
    }

    async Task SetQueryStatusMessageErrorAsync(IMessage queryMsg, string errorStr)
    {
        Embed embed = new Embed(
            Colour: Color.Red,
            Title: errorStr,
            Fields: queryMsg.Embeds[0].Fields);
        await m_channelApi.EditMessageAsync(
            queryMsg.ChannelID,
            queryMsg.ID,
            embeds: new[] { embed });
        await m_channelApi.DeleteAllReactionsAsync(
            queryMsg.ChannelID,
            queryMsg.ID);
    }

    async Task SetQueryStatusMessageDoneAsync(IMessage queryMsg, int currentBookCount, int bookCount)
    {
        await m_channelApi.DeleteAllReactionsAsync(
            queryMsg.ChannelID,
            queryMsg.ID);

        if (bookCount == MAX_BOOK_REPLY_COUNT)
        {
            await m_channelApi.CreateReactionAsync(
                queryMsg.ChannelID,
                queryMsg.ID,
                "➡️");
        }

        currentBookCount += bookCount;
        Embed embed = new Embed(
            Colour: Color.Green,
            Title: "Processing done",
            Description: string.Format(
                "Query returned {0} books.{1}",
                currentBookCount,
                bookCount == 0 ? " No more books result." : ""),
            Fields: queryMsg.Embeds[0].Fields);
        await m_channelApi.EditMessageAsync(
            queryMsg.ChannelID,
            queryMsg.ID,
            embeds: new[] { embed });
    }

    async Task<Result> SendBooksReplyAsync(IMessage queryMsg, int currentBooksCount, NhBook[] books, ILogger logger)
    {
        ++currentBooksCount;
        
        foreach (NhBook book in books)
        {
            ILogger bookLogger = logger.ForContext("book", book);
            Stream? thumbnailStream = null;
            Stream? firstPageStream = null;

            try
            {
                bookLogger.Information("Downloading book cover image");
                thumbnailStream = await m_nhService.DownloadCoverImageAsync(
                    book,
                    default);
                string thumbnailAttachmentName = Path.GetFileName(book.CoverImageUrl);
                FileData thumbnailAttachment = new FileData(
                    thumbnailAttachmentName,
                    thumbnailStream);
                
                bookLogger.Information("Downloading book first page image");
                firstPageStream = await m_nhService.DownloadPageImageAsync(
                    book,
                    1,
                    default);
                string firstPageAttachmentName = Path.GetFileName(book.PageImageUrls[0]);
                FileData firstPageAttachment = new FileData(
                    firstPageAttachmentName,
                    firstPageStream);
                
                bookLogger.Information("Creating book embed message");
                string[] tags = book.Tags
                    .Select(x =>
                    {
                        if (x == "english")
                        {
                            return x + " 🇬🇧";
                        }
                        
                        if (x == "japanese")
                        {
                            return x + " 🇯🇵";
                        }
                        
                        if (x == "chinese")
                        {
                            return x + " 🇨🇳";
                        }

                        if (x == "netorare" ||
                            x == "guro" ||
                            x == "farting")
                        {
                            return x + " ⚠️";
                        }

                        if (x == "lolicon" ||
                            x == "loli")
                        {
                            return x + " 😭";
                        }

                        return x;
                    })
                    .ToArray();
                Embed bookEmbed = new Embed(
                    Colour: Color.Black,
                    Title: string.Format(
                        "{0}. {1}",
                        currentBooksCount++,
                        book.Title),
                    Url: book.SiteUrl,
                    Thumbnail: new EmbedThumbnail("attachment://" + thumbnailAttachmentName),
                    Image: new EmbedImage("attachment://" + firstPageAttachmentName),
                    Fields: new[]
                    {
                        new EmbedField("Id", book.Id),
                        new EmbedField("Author", string.Join(", ", book.Authors)),
                        new EmbedField("Tags", string.Join(", ", tags)),
                        new EmbedField("Release Date", book.ReleaseDate.ToString("yyyy-MM-dd")),
                        new EmbedField("Favorite Count", book.FavoriteCount.ToString()),
                        new EmbedField("Page", string.Format("1/{0}", book.PageImageUrls.Length))
                    });
                Result<IMessage> bookMsgResult = await m_channelApi.CreateMessageAsync(
                    queryMsg.ChannelID,
                    messageReference: new MessageReference(queryMsg.ID),
                    embeds: new[] { bookEmbed },
                    attachments: new[]
                    {
                        OneOf<FileData, IPartialAttachment>.FromT0(thumbnailAttachment),
                        OneOf<FileData, IPartialAttachment>.FromT0(firstPageAttachment)
                    });
                
                if (!bookMsgResult.IsSuccess)
                {
                    bookLogger.Error(
                        new Exception(bookMsgResult.Error.Message),
                        "Error creating book embed message");
                    
                    if (bookMsgResult.Error is not RestResultError<RestError> rer)
                    {
                        return (Result)bookMsgResult;
                    }

                    if (rer.Error.Code != DiscordError.FileUploadExceedsMaximumSize &&
                        rer.Error.Code != DiscordError.RequestEntityTooLarge)
                    {
                        return (Result)bookMsgResult;
                    }

                    bookLogger.Information("Retrying sending book images via link");
                    bookEmbed = bookEmbed with
                    {
                        Thumbnail = new EmbedThumbnail(book.CoverImageUrl),
                        Image = new EmbedImage(book.PageImageUrls[0])
                    };
                    bookMsgResult = await m_channelApi.CreateMessageAsync(
                        queryMsg.ChannelID,
                        messageReference: new MessageReference(queryMsg.ID),
                        embeds: new[] { bookEmbed });
                    
                    if (!bookMsgResult.IsSuccess)
                    {
                        bookLogger.Error(
                            new Exception(bookMsgResult.Error.Message),
                            "Error sending book embed message");
                        return (Result)bookMsgResult;
                    }
                }

                bookLogger.Information("Creating book reply reactions");
                await m_channelApi.CreateReactionAsync(
                    bookMsgResult.Entity.ChannelID,
                    bookMsgResult.Entity.ID,
                    "⛓️");
                await m_channelApi.CreateReactionAsync(
                    bookMsgResult.Entity.ChannelID,
                    bookMsgResult.Entity.ID,
                    "⏮️");
                await m_channelApi.CreateReactionAsync(
                    bookMsgResult.Entity.ChannelID,
                    bookMsgResult.Entity.ID,
                    "⬅️");
                await m_channelApi.CreateReactionAsync(
                    bookMsgResult.Entity.ChannelID,
                    bookMsgResult.Entity.ID,
                    "➡️");
            }
            catch (Exception ex)
            {
                bookLogger.Error(ex, "Error sending book reply");
                return Result.FromError(new ExceptionError(ex));
            }
            finally
            {
                thumbnailStream?.Dispose();
                firstPageStream?.Dispose();
            }
        }

        return Result.FromSuccess();
    }

    [GeneratedRegex(@"\d+")]
    private partial Regex NumberRegex();

    int? GetCurrentBookCount(IEmbed embed)
    {
        if (!embed.Description.HasValue)
        {
            return null;
        }
        
        Match match = NumberRegex().Match(
            embed.Description.Value);

        if (!match.Success)
        {
            return null;
        }

        int result;

        if (!int.TryParse(match.Value, out result))
        {
            return null;
        }

        return result;
    }

    async Task<Result> HandleQueryMessageReactionAsync(IMessageReactionAdd reaction, IMessage message, IEmbed embed, ILogger logger, CancellationToken ct)
    {
        logger.Information("User reacted on search root message");

        if (reaction.Emoji.Name != "➡️")
        {
            logger.Information("Unkown emoji");
            return Result.FromSuccess();
        }

        string? query = embed.Fields.Value
            .Where(x => x.Name == "Query")
            .Select(x => x.Value)
            .FirstOrDefault();
        string? sort = embed.Fields.Value
            .Where(x => x.Name == "Sort")
            .Select(x => x.Value)
            .FirstOrDefault();
        string? type = embed.Fields.Value
            .Where(x => x.Name == "Type")
            .Select(x => x.Value)
            .FirstOrDefault();
        int? currentBookCount = GetCurrentBookCount(embed);
        
        if (currentBookCount == null)
        {
            logger.Error("Invalid search root embed fields");
            return Result.FromSuccess();
        }

        logger = logger
            .ForContext("query", query)
            .ForContext("Sort", sort);
        logger.Information("Setting search root processing message");
        await SetQueryStatusMessageProcessingAsync(message);
        
        NhListResult nhResult;

        try
        {
            logger.Information("Getting search first result page");
            NhListResult firstNhResult;
            bool isLatest = false;
            bool isPopular = false;

            if (!string.IsNullOrEmpty(type))
            {
                if (type == "Latest")
                {
                    firstNhResult = await m_nhService.ListLatestAsync(
                        1,
                        ct);
                    isLatest = true;
                }
                else
                {
                    logger.Error("Invalid search root embed fields");
                    return Result.FromSuccess();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(query))
                {
                    logger.Error("Invalid search root embed fields");
                    return Result.FromSuccess();
                }

                isPopular = sort == "Popular";
                
                if (isPopular)
                {
                    firstNhResult = await m_nhService.SearchPopularAsync(
                        query,
                        1,
                        ct);
                }
                else
                {
                    firstNhResult = await m_nhService.SearchLatestAsync(
                        query,
                        1,
                        ct);
                }
            }

            int searchPageNum = currentBookCount.Value / firstNhResult.BookCountPerPage + 1;

            if (searchPageNum * firstNhResult.BookCountPerPage <= currentBookCount.Value)
            {
                ++searchPageNum;
            }

            logger = logger.ForContext("searchPage", searchPageNum);
            logger.Information("Getting search page result");

            if (isLatest)
            {
                nhResult = await m_nhService.ListLatestAsync(
                    searchPageNum,
                    ct);
            }
            else if (!string.IsNullOrEmpty(query))
            {
                if (isPopular)
                {
                    nhResult = await m_nhService.SearchPopularAsync(
                        query,
                        searchPageNum,
                        ct);
                }
                else
                {
                    nhResult = await m_nhService.SearchLatestAsync(
                        query,
                        searchPageNum,
                        ct);
                }
            }
            else
            {
                // Impossible.
                logger.Error("Invalid search root embed fields");
                return Result.FromSuccess();
            }
        }
        catch (TaskCanceledException)
        {
            logger.Error("Operation cancelled");
            await SetQueryStatusMessageErrorAsync(
                message,
                "Operation cancelled");
            return Result.FromSuccess();
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Unable to search books");
            await SetQueryStatusMessageErrorAsync(
                message,
                "Unable to load more search results at the moment");
            return Result.FromError(new ExceptionError(ex));
        }

        logger.Information("Setting query status message done");
        NhBook[] books = nhResult.Books
            .Skip(currentBookCount.Value % nhResult.BookCountPerPage)
            .Take(MAX_BOOK_REPLY_COUNT)
            .ToArray();
        await SetQueryStatusMessageDoneAsync(
            message,
            currentBookCount.Value,
            books.Length);
        
        logger.Information("Sending books reply");
        return await SendBooksReplyAsync(
            message,
            currentBookCount.Value,
            books,
            logger);
    }

    async Task<Result> HandleRelatedBookReactionAsync(IMessageReactionAdd reaction, IMessage message, IEmbed embed, ILogger logger)
    {
        string? bookId = embed.Fields.Value
            .Where(x => x.Name == "Id")
            .Select(x => x.Value)
            .FirstOrDefault();

        if (string.IsNullOrEmpty(bookId))
        {
            logger.Error("Invalid book embed");
            return Result.FromSuccess();
        }
        
        CancellationToken ct = m_channelRepository.GetCancellationToken(
            message.ChannelID.Value,
            default);

        try
        {
            logger = logger
                .ForContext("type", "related")
                .ForContext("bookId", bookId);
            logger.Information("Sending query status message");
            Result<IMessage> queryMsgResult = await CreateRelatedQueryStatusMessageAsync(
                message,
                bookId);
            
            if (!queryMsgResult.IsSuccess)
            {
                logger.Error(
                    new Exception(queryMsgResult.Error.Message),
                    "Error sending query status message");
                return (Result)queryMsgResult;
            }

            NhListResult nhResult;

            try
            {
                logger.Information("Getting related books");
                nhResult = await m_nhService.GetRelatedBooksAsync(
                    bookId,
                    ct);
            }
            catch (TaskCanceledException)
            {
                logger.Error("Operation cancelled");
                await SetQueryStatusMessageErrorAsync(
                    queryMsgResult.Entity,
                    "Operation cancelled");
                return Result.FromSuccess();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to list related books");
                await SetQueryStatusMessageErrorAsync(
                    queryMsgResult.Entity,
                    "Unable to retrieve related books at the moment");
                return Result.FromError(new ExceptionError(ex));
            }
            
            if (nhResult.Books.Length == 0)
            {
                logger.Error("No releated books");
                await SetQueryStatusMessageErrorAsync(
                    queryMsgResult.Entity,
                    "No related books");
                return Result.FromSuccess();
            }
            
            logger.Information("Setting query status message done");
            await SetRelatedQueryStatusMessageDoneAsync(
                queryMsgResult.Entity,
                nhResult.Books.Length);

            logger.Information("Sending books reply");
            return await SendBooksReplyAsync(
                queryMsgResult.Entity,
                0,
                nhResult.Books,
                logger);
        }
        finally
        {
            m_channelRepository.DestroyCancellationToken(
                message.ChannelID.Value);
            
            logger.Information("Undoing message reaction");
            await m_channelApi.DeleteUserReactionAsync(
                reaction.ChannelID,
                reaction.MessageID,
                reaction.Emoji.Name.Value ?? "",
                reaction.UserID);
        }
    }
    
    async Task<Result> HandleBookReactionAsync(IMessageReactionAdd reaction, IMessage message, IEmbed embed, ILogger logger)
    {
        logger.Information("User reacted on book message");
        int adder;
        
        if (reaction.Emoji.Name == "⬅️")
        {
            adder = -1;
        }
        else if (reaction.Emoji.Name == "➡️")
        {
            adder = 1;
        }
        else if (reaction.Emoji.Name == "⏮️")
        {
            adder = int.MinValue;
        }
        else if (reaction.Emoji.Name == "⛓️")
        {
            return await HandleRelatedBookReactionAsync(
                reaction,
                message,
                embed,
                logger);
        }
        else
        {
            logger.Information("Unknown emoji");
            return Result.FromSuccess();
        }

        string? bookId = embed.Fields.Value
            .Where(x => x.Name == "Id")
            .Select(x => x.Value)
            .FirstOrDefault();
        string? bookPage = embed.Fields.Value
            .Where(x => x.Name == "Page")
            .Select(x => x.Value)
            .FirstOrDefault();
        string? bookTags = embed.Fields.Value
            .Where(x => x.Name == "Tags")
            .Select(x => x.Value)
            .FirstOrDefault();
        string? bookAuthors = embed.Fields.Value
            .Where(x => x.Name == "Author")
            .Select(x => x.Value)
            .FirstOrDefault();
        string? bookReleaseDate = embed.Fields.Value
            .Where(x => x.Name == "Release Date")
            .Select(x => x.Value)
            .FirstOrDefault();

        if (bookId == null ||
            bookPage == null ||
            bookTags == null ||
            bookAuthors == null ||
            bookReleaseDate == null)
        {
            logger.Information("Invalid reacted message embed");
            return Result.FromSuccess();
        }

        int currentPage = int.Parse(bookPage.Split('/').First());

        if (adder == int.MinValue)
        {
            currentPage = 1;
        }
        else
        {
            currentPage += adder;
        }
        
        logger = logger
            .ForContext("bookId", bookId)
            .ForContext("currentPage", currentPage);
        Stream? pageStream = null;
        
        try
        {
            logger.Information("Getting book information");
            NhBook? book = await m_nhService.GetBookAsync(
                bookId,
                default);

            if (book == null ||
                currentPage > book.PageImageUrls.Length ||
                currentPage == 0)
            {
                logger.Error("No book or invalid page");
                return Result.FromSuccess();
            }
            
            logger.Information("Downloading next page image");
            pageStream = await m_nhService.DownloadPageImageAsync(
                book,
                currentPage,
                default);
            m_cacheService.AddCacheJob(book, currentPage + 1);
            string pageAttachmentName = Path.GetFileName(
                book.PageImageUrls[currentPage - 1]);
            FileData pageAttachment = new FileData(
                pageAttachmentName,
                pageStream);
            
            m_logger.Information("Setting book page");
            Embed bookEmbed = new Embed(
                Colour: embed.Colour,
                Title: embed.Title,
                Url: embed.Url,
                Thumbnail: embed.Thumbnail,
                Image: new EmbedImage("attachment://" + pageAttachmentName),
                Fields: new[]
                {
                    new EmbedField("Id", book.Id),
                    new EmbedField("Author", bookAuthors),
                    new EmbedField("Tags", bookTags),
                    new EmbedField("Release Date", bookReleaseDate),
                    new EmbedField("Favorite Count", book.FavoriteCount.ToString()),
                    new EmbedField(
                        "Page",
                        string.Format(
                            "{0}/{1}",
                            currentPage,
                            book.PageImageUrls.Length))
                });
            Result<IMessage> bookMsgResult = await m_channelApi.EditMessageAsync(
                message.ChannelID,
                message.ID,
                embeds: new[] { bookEmbed },
                attachments: new[]
                {
                    OneOf<FileData, IPartialAttachment>.FromT0(pageAttachment)
                });
            
            if (!bookMsgResult.IsSuccess)
            {
                logger.Error(
                    new Exception(bookMsgResult.Error.Message),
                    "Error setting book page");
                
                if (bookMsgResult.Error is not RestResultError<RestError> rer)
                {
                    return (Result)bookMsgResult;
                }

                if (rer.Error.Code != DiscordError.FileUploadExceedsMaximumSize &&
                    rer.Error.Code != DiscordError.RequestEntityTooLarge)
                {
                    return (Result)bookMsgResult;
                }

                logger.Information("Retrying setting book page via image link");
                bookEmbed = bookEmbed with
                {
                    Image = new EmbedImage(book.PageImageUrls[currentPage - 1])
                };
                bookMsgResult = await m_channelApi.EditMessageAsync(
                    message.ChannelID,
                    message.ID,
                    embeds: new[] { bookEmbed });
                
                if (!bookMsgResult.IsSuccess)
                {
                    logger.Error(
                        new Exception(bookMsgResult.Error.Message),
                        "Error setting book page");
                    return (Result)bookMsgResult;
                }
            }
            
            logger.Information("Undoing message reaction");
            await m_channelApi.DeleteUserReactionAsync(
                reaction.ChannelID,
                reaction.MessageID,
                reaction.Emoji.Name.Value!,
                reaction.UserID);
            
            return Result.FromSuccess();
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Error setting book page");
            return Result.FromError(new ExceptionError(ex));
        }
        finally
        {
            pageStream?.Dispose();
        }
    }
    
    void HandleSearchCancelReaction(IMessageReactionAdd reaction, ILogger logger)
    {
        if (reaction.Emoji.Name != "🛑")
        {
            logger.Information("Unknown emoji");
            return;
        }

        logger.Information("Cancelling channel's search operation");
        m_channelRepository.CancelChannelOperation(
            reaction.ChannelID.Value);
    }

    public async Task<Result> RespondAsync(IMessageReactionAdd reaction, CancellationToken ct = default)
    {
        if (reaction.Member.HasValue &&
            reaction.Member.Value.User.HasValue &&
            reaction.Member.Value.User.Value.IsBot.HasValue &&
            reaction.Member.Value.User.Value.IsBot.Value)
        {
            // Reactor is a bot.
            return Result.FromSuccess();
        }
        
        ILogger logger = m_logger.GetLoggerContext(reaction);
        Result<IMessage> msgResult = await m_channelApi.GetChannelMessageAsync(
            reaction.ChannelID,
            reaction.MessageID,
            ct);

        if (!msgResult.IsSuccess)
        {
            // Cant get reacted message info.
            return (Result)msgResult;
        }

        if (msgResult.Entity.Author.ID != m_state.BotId)
        {
            // Not our message.
            return Result.FromSuccess();
        }

        if (msgResult.Entity.Embeds.Count != 1 ||
            !msgResult.Entity.Embeds[0].Fields.HasValue)
        {
            // Invalid embed.
            return Result.FromSuccess();
        }

        IEmbed embed = msgResult.Entity.Embeds[0];
        ct = m_channelRepository.GetCancellationToken(
            reaction.ChannelID.Value,
            ct);

        try
        {
            if (embed.Title == "Processing done")
            {
                return await HandleQueryMessageReactionAsync(
                    reaction,
                    msgResult.Entity,
                    embed,
                    logger,
                    ct);
            }
            else if (
                embed.Title == "Processing your request" ||
                embed.Title == "Getting more search results")
            {
                HandleSearchCancelReaction(
                    reaction,
                    logger);
                return Result.FromSuccess();
            }
            else
            {
                return await HandleBookReactionAsync(
                    reaction,
                    msgResult.Entity,
                    embed,
                    logger);
            }
        }
        finally
        {
            m_channelRepository.DestroyCancellationToken(
                reaction.ChannelID.Value);
        }
    }
}
