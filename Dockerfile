# dotnet sdk.
FROM mcr.microsoft.com/dotnet/sdk:8.0.100-bookworm-slim-amd64 as build
RUN dpkg --add-architecture arm64 && \
    apt-get update && \
    apt-get install -y \
    clang zlib1g-dev zlib1g-dev:arm64 \
    binutils-aarch64-linux-gnu gcc-aarch64-linux-gnu
COPY . /build
WORKDIR /build/discord-bot-nh-reader
ARG TARGETPLATFORM
RUN ARCH=$(echo $TARGETPLATFORM | cut -d '/' -f 2) && \
    PREFIX=$(echo $ARCH | awk '{if ($0 == "arm64") print "aarch64-linux-gnu-";}') && \
    dotnet publish \
    -c Release \
    -r linux-$ARCH \
    -p:ObjCopyName=${PREFIX}objcopy \
    -p:PublishReadyToRun=true \
    -o /output

# dotnet runtime.
FROM mcr.microsoft.com/dotnet/runtime:8.0.0-bookworm-slim as runtime
COPY --from=build /output /app

# Entry point.
ENTRYPOINT [ "/app/discord-bot-nh-reader" ]
